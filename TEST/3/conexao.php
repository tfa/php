<?php 
    define('HOST', 'localhost');
    define('USER', 'root');
    define('PASS', '');
    define('DBNAME', 'marknet_db');

    try
    {
        $PDO = new pdo('mysql:host=' . HOST . ';dbname=' . DBNAME, USER, PASS);
    }
    catch ( PDOException $e )
    {
        echo 'Erro ao conectar com o MySQL: ' . $e->getMessage();
    }
?>