<?php

class MyDate {

    static function toAmerican(String $brDate)
    {
        return date("Y-m-d", strtotime(implode("-",array_reverse(explode("/",$brDate)))));
    }

    static function toBrazilian(String $euaDate)
    {
        return date("d/m/Y", strtotime($euaDate));
    }

    static function toggle(String $date)
    {
        if ( preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/[0-9]{4}$/",$date)) {
            /* CONVERTER pra AMERICANO */
            $date = MyDate::toAmerican($date);
        } else if(preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
                /* CONVERTER pra BRASILEIRO */
            $date = MyDate::toBrazilian($date);
        }else{
            throw new Exception('Formato de data inválido');
        }

        return $date; 
    }
}

try{
    $exData = new MyDate();
    var_dump( $exData->toggle(date('Y-m-d')));
}catch(Exception $e)
{
   echo $e->getMessage(); 
}
?>